package testsdemo;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
public class Testseleniumjenkins {
public static void main(String[] args) {
		//Setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver", "E:\\software\\cd\\chromedriver.exe");
//Creating an object of ChromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
//Deleting all the cookies
		driver.manage().deleteAllCookies();
//Specifiying pageLoadTimeout and Implicit wait
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//launching the specified URL
		driver.get("http://104.198.155.142:4201/#/");
		driver.findElement(By.id("email"));
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys("chitrakshi.advance@example.com");
		// Thread.sleep(3000);
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys("1");
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys("2");
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys("3");
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys("4");
		driver.findElement(By.className("globalButton")).submit();
		WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		nav_btn.click();
		WebElement user = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[13]/span"));
		user.click();
		driver.close();
		System.out.println("Successfully verified click-on UserSetting Button :- test-case pass");
	}
	}